import { getTimestamp } from './utils';

describe('getTimestamp', () => {
  it('should return the current timestamp in the expected format', () => {
    const fixedDate = new Date('2023-07-23T12:34:56.789Z');

    jest.useFakeTimers();
    jest.setSystemTime(fixedDate.getTime());

    const timestamp = getTimestamp();
    expect(timestamp).toBe('2023-07-23T12:34:56.789Z');

    jest.useRealTimers();
  });
});
