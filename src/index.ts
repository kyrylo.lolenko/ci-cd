import dotenv from 'dotenv';
import express, { Express, Request, Response } from 'express';

import { getTimestamp } from './utils';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;

app.get('/', (req: Request, res: Response) => {
  res.send(`Express + Typescript server. Timestamp: ${getTimestamp()}`);
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
