FROM node:18-alpine3.17
WORKDIR /usr/app
COPY ./ /usr/app
RUN npm install
RUN npm run build
EXPOSE 8001
CMD ["npm", "start"]
